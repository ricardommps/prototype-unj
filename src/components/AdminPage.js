import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';

import Header from './header/Header'
import ClippedDrawer from './drawer/drawer'
import Content from './content/Content'

import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import { fetchTags, createTag } from '../actions/tagActions'
import { fetchCards, updateCard } from '../actions/cards'
import { cloneDeep } from 'lodash';
import {styles} from './style'

function matcher(regexp) {
    return function (obj) {
    var found = false;
    Object.keys(obj).forEach(function(key){
      if ( ! found) {
        if ((typeof obj[key] == 'string') && regexp.exec(obj[key])) {
          found = true;
        }
      }
    });
    return found;
    };
}

function filterPlainArray (array, filters)  {
    var matches = []
    filters.forEach(function(needle) {
        var re1 = new RegExp("\\b" + needle + "\\b", 'i');
        matches = array.filter(matcher(re1));
    });
    return matches
}

function filterArray(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter(item => {
        return filterKeys.every(key => {
            if (typeof filters[key] !== 'function') return true;
            return filters[key](item[key]);
        });
    });
  }
  
  

class AdminPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            filterTagById: null,
            filterTag:"",
            search: false
        }
    }

    componentDidMount() {
        this.props.fetchTags()
        this.props.fetchCards()
      }

    handleTagtoCard = (card, id) => {
        this.props.updateCard(card,id)
    }

    handleCreateTag = (tag) => {
        this.props.createTag(tag)
    }

    handleClickFilterTag = (id) => {
        if(!id){
            this.setState({
                search:false
            })
        }
        this.setState({
            filterTagById:id
        })
    }

    handleFilterTag = () => {
        const { filterTag } = this.state
        if(filterTag.length > 0) {
            this.setState({
                search:true
            })
        }else{
            this.setState({
                search:false
            })
        }

    }

    searchItems = () => {
        const cards = this.props.cards
        const { filterTag } = this.state
        if(filterTag.length > 0){
            const arrayFilter = [filterTag]
            const filtered = filterPlainArray(cards, arrayFilter);
            const filterTagById  = this.state.filterTagById
            if(filterTagById) {
                const filtersTag = {
                    tag: tag => tag.find(x => [parseInt(filterTagById, 10)].includes(x)),
                };
                const filteredByTag =this.filteredItens(filtered, filtersTag);
                return filteredByTag
            }
            return filtered
        }
    }

    handleChangeFilter = (event) => {
        
        this.setState({
            filterTag: event.target.value,
            search:false
        })
    }

    countTagsByCard = (id) => {
        const cards = this.props.cards
        const filters = {
            tag: tag => tag.find(x => [parseInt(id, 10)].includes(x)),
        };
        const filtered = filterArray(cards, filters);
        return filtered.length
    }

    filterTagById = () => {
        let cards = [];
        const {search} = this.state
        if(search) {
            cards = this.searchItems()
        }else{
            cards = this.props.cards;
        }
        const filterTagById  = this.state.filterTagById
        const filters = {
            tag: tag => tag.find(x => [parseInt(filterTagById, 10)].includes(x)),
        };
        if(filterTagById){
            const filtered = this.filteredItens(cards, filters);
            return filtered
        }
       
        return cards
    }

    filteredItens = (cards, filters) => {
        const filtered = filterArray(cards, filters);
        return filtered
    }

    render() {
        const { classes, tags } = this.props
        let cards = this.filterTagById()
        return (
            <div className={classes.root}>
                <CssBaseline />
                <Header/>
                <ClippedDrawer 
                    tags={tags} 
                    handleCreateTag={this.handleCreateTag} 
                    handleClickFilterTag={this.handleClickFilterTag}
                    countTagsByCard={this.countTagsByCard}
                />
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <div className={classes.searchBar}>
                        <IconButton 
                            className={classes.iconButton} 
                            aria-label="search"  
                            onClick={() => this.handleFilterTag()}>
                            <SearchIcon />
                        </IconButton>
                        <InputBase
                            className={classes.input}
                            placeholder="Buscar"
                            inputProps={{ 'aria-label': 'Buscar' }}
                            onChange={(event)=>this.handleChangeFilter(event)}
                        />
                    </div>
                    <div className={classes.cards}> 
                        {cards.length > 0 ?
                            <Content tags={tags} cards={cards} handleTagtoCard={this.handleTagtoCard}/>
                            :<div><span>Nenhum item encontrado</span></div>
                        }
                    </div>
                </main>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    tags: state.tags,
    cards: state.cards
});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            fetchTags,
            fetchCards,
            updateCard,
            createTag
        },
        dispatch
);

export default compose(
    withStyles(styles),
    connect(mapStateToProps, mapDispatchToProps),
)(AdminPage);