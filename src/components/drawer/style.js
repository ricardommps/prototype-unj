export const styles = theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    tagTitle: {
        fontSize: '14px'
    },
    shape: {
        backgroundColor: theme.palette.primary.main,
        width: 16,
        height: 6,
        marginRight: '10px',
        marginLeft: '4px'
    },
    button: {
        padding: 0
    },
    backspaceIcon: {
        transform: 'rotate(180deg)',
        width: 16,
        height: 16,
        cursor:'pointer',
        marginRight: theme.spacing(1),
      },
    iconSmall: {
        fontSize: 20,
    },
    input: {
        flex: 1,
        border: '1px solid #0078D7',
        opacity: 1,
        paddingLeft: '9px',
        textAlign: 'left',
        letterSpacing: 0,
        color: '#333333',
        opacity: 1,
        fontSize: '14px'
    },
    iconButton: {
        padding: 10,
    },
    labelEtiquetas:{
        marginLeft: '16px',
        marginTop: '20px',
        marginBottom: '12px'
    },
    labelProcessos:{
        marginLeft: '16px',
        marginTop: '12px',
        marginBottom: '12px'
    },
    listProcessos: {
        backgroundColor: '#EAEAEA',
        borderLeftStyle: 'solid',
        borderLeftWidth: '5px',
        borderLeftColor: '#1B3D77'
    },
    itemProcesso: {
        margin:0
    },
    list:{
        paddingBottom: 0,
        paddingTop: 0,
    },
    listItemProcessos: {
        paddingBottom: 3,
        paddingTop: 3,
    },
    saveButton: {
        fontFamily: '"Lato", sans-serif',
        color: '#808080',
        fontSize: '14px',
        fontWeight: '400',
        lineHeight: '1.5',
        letterSpacing: '0.00938em'
    },
    typography: {
        fontSize: '12px',
        paddingTop: '3px'
      },

})

const drawerWidth = 220;