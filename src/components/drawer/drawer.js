import React, { Component, Fragment } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import BackspaceOutlinedIcon from '@material-ui/icons/BackspaceOutlined';
import Button from "@material-ui/core/Button";
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import DirectionsIcon from '@material-ui/icons/Directions';
import Typography from '@material-ui/core/Typography';

import {styles} from './style'

class ClippedDrawer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showInputTag: null,
            tagText:""
        }
    }

    handleShowInputTag = (event) => {
        const { showInputTag } = this.state
        this.setState({
            showInputTag: showInputTag ? null : event.currentTarget,
        })
    }

    handleSaveTag = () => {
        this.saveTagItem()

    }

    keyPress(e){
        if(e.which == 13){
            this.saveTagItem()
        }
    }

    saveTagItem = () => {
        const { tagText } = this.state
        this.setState({
            showInputTag: null
        })
        const tagItem = {
            id: this.rollTheDice(),
            name:tagText,
            color: '#fff',
            background: this.generateColor()
        }
        this.props.handleCreateTag(tagItem)
    }

    rollTheDice = () => {
        return Math.floor(100 + Math.random() * 900).toString()
      }

    handleChangeTag = (event) => {
        this.setState({
            tagText: event.target.value
        })
    }

    generateColor () {
        return '#' +  Math.random().toString(16).substr(-6);
    }
    render() {
        const { classes, tags, handleClickFilterTag, countTagsByCard } = this.props
        const { showInputTag } = this.state
        return (
            <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                paper: classes.drawerPaper,
                }}
            >
                <div className={classes.toolbar} />
                <span className={classes.labelProcessos}>Processos</span>
                <List className={`${classes.listProcessos} ${classes.list}`}>
                    <ListItem button onClick={() => handleClickFilterTag()}>
                        <ListItemText primary="Todos Processos" className={classes.itemProcesso}/>
                    </ListItem>
                </List>
                <span className={classes.labelEtiquetas}>Etiquetas</span>
                <List className={classes.list}>
                {tags.map((tag,index) => (
                    <ListItem 
                        button key={tag.id} 
                        onClick={() => handleClickFilterTag(tag.id)} 
                        className={classes.listItemProcessos}
                    >
                        <div className={classes.shape} style={{ backgroundColor: tag.background }}></div>
                        <ListItemText primary={tag.name}  className={classes.tagTitle}/>
                        <Typography className={classes.typography}>
                            {countTagsByCard(tag.id)}
                        </Typography>          
                    </ListItem>
                ))}

                <ListItem>
                    { !showInputTag ? 
                        <Button 
                            size="small" 
                            className={classes.saveButton} 
                            style={{ display: showInputTag }}
                            onClick={(event) => this.handleShowInputTag(event)}
                        >
                            <BackspaceOutlinedIcon className={classes.backspaceIcon} />
                            Save
                        </Button>
                        :
                        <Fragment>
                            <InputBase
                                className={classes.input}
                                placeholder="Tag"
                                onKeyPress={(event) => this.keyPress(event)}
                                onChange={(event)=>this.handleChangeTag(event)}
                                autoFocus={true}
                            />
                            <IconButton 
                                color="primary" 
                                className={classes.iconButton}
                                onClick={() => this.handleSaveTag()} 
                            >
                                <DirectionsIcon />
                            </IconButton>
                        </Fragment>
                    }
                    
                </ListItem>
                </List>
            </Drawer>
        )
    }
}

export default withStyles(styles) (ClippedDrawer)