export const styles = theme => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        boxShadow: 'none',
    },
    toolbar: theme.mixins.toolbar,
    title: {
        flexGrow: 1,
    }
})