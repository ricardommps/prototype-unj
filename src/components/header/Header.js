import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';

import {styles} from './style'

class Header extends Component {
    render() {
        const { classes } = this.props
        return (
            <AppBar position="fixed" className={classes.appBar} color="primary">
                <Toolbar variant="dense">
                    <Typography variant="h6" className={classes.title}>
                        App
                    </Typography>
                    
                    <IconButton
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        color="inherit"
                    >
                        <AccountCircle />
                    </IconButton>
                </Toolbar>
            </AppBar>
        )
    }
}

export default withStyles(styles) (Header)