import React, { Component, Fragment } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Popper from '@material-ui/core/Popper';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';

import ClearIcon from '@material-ui/icons/Clear';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import FolderOpenIcon from '@material-ui/icons/FolderOpen';

import BackspaceOutlinedIcon from '@material-ui/icons/BackspaceOutlined';

import ButtonBase from '@material-ui/core/ButtonBase';
import Divider from '@material-ui/core/Divider';

import { cloneDeep } from 'lodash';

import { styles } from './style'

class Content extends Component {

    constructor(props) {
        super(props)
        this.state = {
            anchorEl: null,
            placement: 'right-end',
            tagsButtons: [],
            cardId: null
        }
    }

    getItensTag = (cardTag) => {
        const { tags } = this.props
        const newCardTag = cardTag.map(String)
        const filters = {
            id: id => !newCardTag.includes(id),
        };
        const filtered = this.filterArray(tags, filters);
        this.setState({
            tagsButtons: filtered
        })
    }

    getTagName = (tagId) => {
        const { tags } = this.props
        let tag = null
        tags.map(item => {
            if (item.id == tagId) {
                tag = item
            }
        })
        return tag
    }


    handleClick = (card, event) => {
        const { anchorEl } = this.state
        this.setState({
            anchorEl: anchorEl ? null : event.currentTarget,
            cardId: card.id
        })
        this.getItensTag(card.tag)
    }

    handleClose = () => {
        this.setState({
            anchorEl: null,
            cardId: null
        })
    }

    handleDeleteTag = (tagIndex, cardIndex) => {
        const cards = cloneDeep(this.props.cards);
        let cardId = cards[cardIndex].id
        cards[cardIndex].tag.splice(parseInt(tagIndex, 10), 1)
        this.props.handleTagtoCard(cards[cardIndex], cardId)
    }

    handleTagSelected = (tagId) => {
        const { cardId } = this.state
        const cards = cloneDeep(this.props.cards);
        let idxCardSelected = cards.findIndex(x => x.id === cardId);
        let idx = cards[idxCardSelected].tag.indexOf(tagId)
        this.setState({ anchorEl: null })
        if (idx === -1) {
            cards[idxCardSelected].tag.push(parseInt(tagId, 10))
            this.props.handleTagtoCard(cards[idxCardSelected], cardId)
        }
    }
    filterArray = (array, filters) => {
        const filterKeys = Object.keys(filters);
        return array.filter(item => {
            return filterKeys.every(key => {
                if (typeof filters[key] !== 'function') return true;
                return filters[key](item[key]);
            });
        });
    }

    render() {
        const { classes, cards, tags } = this.props
        const { anchorEl, placement, tagsButtons } = this.state
        const open = Boolean(anchorEl);
        return (
            <div className={classes.root}>

                {cards.map((card, index) => (
                    <Paper className={classes.paper} key={index}>
                        <Grid container spacing={0}>
                            <Grid item sm={5} container>
                                <Grid item xs container direction="column" spacing={3}>
                                    <Grid item xs>
                                        <Typography className={classes.typography}>
                                            <AddCircleIcon className={classes.addCircleIcon} />
                                            {card.partes.ativa.name}<strong>{card.partes.ativa.plus}</strong>
                                            <RemoveCircleIcon className={classes.removeCircleIcon} />
                                            {card.partes.passiva.name}<strong>{card.partes.passiva.plus}</strong>
                                        </Typography>
                                        <Typography gutterBottom className={`${classes.typography} ${classes.subTitle}`}>
                                            {card.classe} - <strong>{card.assunto}</strong>
                                        </Typography>
                                        <Typography className={classes.typography}>
                                            {card.numero}
                                            {card.tarja.map((tarja, keyTarja) => {
                                                if(tarja.name.length > 0) {
                                                    return (
                                                        <Chip
                                                            key={keyTarja}
                                                            label={tarja.name}
                                                            className={classes.tarja}
                                                            style={{
                                                                backgroundColor: tarja.background,
                                                                color: tarja.color
                                                            }}
                                                        />
                                                    )
                                                }
                                            })}

                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <ButtonBase className={classes.columFolder} disableRipple disableTouchRipple disableFocusRibble>
                                    <Typography className={classes.typography}>
                                        <FolderOpenIcon className={classes.folderOpenIcon}/>
                                        Abrir pasta
                                    </Typography>
                                </ButtonBase>
                            </Grid>

                            <Grid item>
                                <ButtonBase className={classes.columButtomTag} disableRipple disableTouchRipple disableFocusRibble >
                                    {tags.length > 0 && <BackspaceOutlinedIcon className={classes.backspaceIcon} onClick={(event) => this.handleClick(card, event)} />}
                                    
                                </ButtonBase>
                            </Grid>

                            <Grid item className={classes.tagSelected} sm={3}>
                                {tags.length > 0 && card.tag.map((item, key) => {
                                    return (
                                        <Chip
                                            key={key}
                                            deleteIcon={<ClearIcon className={classes.clearIcon} />}
                                            label={this.getTagName(item).name}
                                            onDelete={() => this.handleDeleteTag(key, index)}
                                            className={classes.chip}
                                            style={{
                                                backgroundColor: this.getTagName(item).background,
                                                color: this.getTagName(item).color
                                            }}
                                        />
                                    )
                                })}
                            </Grid>
                        </Grid>
                    </Paper>
                ))}
                {tagsButtons.length > 0 &&
                    <Popper
                        open={open}
                        anchorEl={anchorEl}
                        placement="bottom-start"
                        disablePortal={false}
                        modifiers={{
                            flip: {
                                enabled: true,
                            },
                            preventOverflow: {
                                enabled: true,
                                boundariesElement: 'scrollParent',
                            }
                        }}
                        transition>
                        {({ TransitionProps }) => (
                            <Fade {...TransitionProps} timeout={350}>
                                <Paper className={classes.paper}>
                                    {tagsButtons.map((item, index) => (
                                        <Button
                                            key={index}
                                            variant="contained"
                                            className={classes.tagButton}
                                            style={{
                                                backgroundColor: item.background,
                                                color: item.color
                                            }}
                                            onClick={() => this.handleTagSelected(item.id)}
                                        >
                                            {item.name}
                                        </Button>
                                    ))}
                                </Paper>
                            </Fade>
                        )}
                    </Popper>
                }
            </div>
        )
    }
}

export default withStyles(styles)(Content)