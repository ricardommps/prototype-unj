export const styles = theme => ({
    card: {
        minWidth: 275,
        padding: theme.spacing(2),
      },
      bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
      },
      title: {
        fontSize: 14,
      },
      pos: {
        marginBottom: 12,
      },
      iconButton: {
        margin: theme.spacing(1),
      },
      paper: {
        padding: theme.spacing(2),
        marginBottom: '10px'
      },
      button: {
        margin: theme.spacing(1),
      },
      chip: {
        margin: theme.spacing(0.5),
        borderRadius: '0px',
        fontSize: '12px',
        height:'23px'
      },
      tarja: {
        margin: theme.spacing(0.5),
        fontSize: '12px',
        height:'16px'
      },
      tagButton: {
        display: 'flex',
        margin: theme.spacing(1),
        width: '167px',
        height: '25px',
        backgroundColor: theme.palette.primary.main,
        padding: 0,
        color: 'white',
        justifyContent: 'flex-start',
        paddingLeft: '10px',
        boxShadow: 'none',
        "&:hover": {
          backgroundColor: theme.palette.primary.main,
        }
      },
      clearIcon : {
        color: 'white',
        height: '13px',
        margin: 0,
        "&:hover": {
          color: 'white',
        }
      },

      addCircleIcon: {
        height: '15px',
        width: '15px',
        position: 'relative',
        top: '3px',
        color: '#01AE76',
        marginRight: '5px'
      },

      removeCircleIcon: {
        height: '15px',
        width: '15px',
        position: 'relative',
        top: '3px',
        color: '#FF5766',
        marginLeft: '10px',
        marginRight: '5px'
      },

      root: {
        flexGrow: 1,
      },

      columFolder: {
        width: 195,
        height: 68,
        borderLeft: '2px solid rgba(0, 0, 0, 0.12)',
        cursor:'auto',
        "&:focus": {
          background: "transparent"
        }
      }, 

      columButtomTag: {
        width: 70,
        height: 68,
        borderLeft: '2px solid rgba(0, 0, 0, 0.12)',
        cursor:'auto',
        "&:focus": {
          background: "transparent"
        }
       
      },

      typography: {
        fontSize: '12px'
      },

      subTitle: {
        color:'#4D4D4D',
        marginBottom: 0
      },

      backspaceIcon: {
        transform: 'rotate(180deg)',
        width: 16,
        height: 16,
        cursor:'pointer'
      },
      folderOpenIcon: {
        width: 18,
        height: 18,
        cursor:'pointer',
        position: 'relative',
        top: '3px',
        marginRight: '5px'
      },
      tagSelected : {
        paddingTop: '19px'
      }

})