export const styles = theme => ({
      tagButton: {
        margin: theme.spacing(1),
        width: '167px',
        height: '25px',
        backgroundColor: theme.palette.primary.main,
        padding: 0,
        color: 'white',
        justifyContent: 'flex-start',
        paddingLeft: '10px',
        boxShadow: 'none',
        "&:hover": {
          //you want this to be the same as the backgroundColor above
          backgroundColor: theme.palette.primary.main,
        }
      }
})