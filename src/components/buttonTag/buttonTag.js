import React, { Component, Fragment } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import {styles} from './style'

class ButtonTag extends Component {

    render() {
        const { classes,cardTags } = this.props
        return (
           <Fragment>
               {cardTags.map((item,index) => (
                   <Button variant="contained" className={classes.tagButton}>
                        Default
                    </Button>  
               ))}
           </Fragment>
        )
    }
}

export default withStyles(styles) (ButtonTag)