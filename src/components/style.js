export const styles = theme => ({
    root: {
        display: 'flex'
    },
    content: {
        flexGrow: 1,
        background: '#EAEAEA',
        width: '100%',
        minHeight: '100%',
        position: 'absolute',
        paddingLeft: '219px'
    },
    toolbar: {
      minHeight: '50px !important'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
      },
      iconButton: {
        padding: 10,
      },
      divider: {
        height: 28,
        margin: 4,
      },
      cards:{
        padding: theme.spacing(3),
      },
      searchBar: {
        backgroundColor: '#F9F9F9'
      }
})