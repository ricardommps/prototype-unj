import { createMuiTheme } from '@material-ui/core/styles';


export default createMuiTheme({
      overrides: {
        MuiTypography: {
          body1: {
            fontFamily: '"Lato", sans-serif',
            color: '#808080',
            fontSize: '14px'
          }
        }
    },
    palette: {
      primary: {
        light: '#757ce8',
        main: '#0063B1',
        dark: '#002884',
        contrastText: '#fff',
      },
      secondary: {
        light: '#ff7961',
        main: '#f44336',
        dark: '#ba000d',
        contrastText: '#000',
      },
    }
});
