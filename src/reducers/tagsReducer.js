import { GET_TAGS, CREATE_TAG, UPDATE_TAG, DELETE_TAG } from '../actions/actionTypes'
  
export default function tagsReducer(state = [], action) {
    switch (action.type) {
      case GET_TAGS:
        return [...state, ...action.payload]
      case CREATE_TAG:
        return [...state, action.payload]
      case UPDATE_TAG:
        const indexOfUpdatedTag = state.findIndex(
          tag => tag.id === action.payload.id
        )
        state.splice(indexOfUpdatedTag, 1)
        state.splice(indexOfUpdatedTag, 0, action.payload)
        return [...state]
      case DELETE_TAG:
        const indexOfDeletedTag = state.findIndex(
          tag => tag.id === action.id
        )
        state.splice(indexOfDeletedTag, 1)
        return [...state]
      default:
        return state
    }
}