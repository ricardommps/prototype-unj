import { GET_CARDS, CREATE_CARD, UPDATE_CARD, DELETE_CARD } from '../actions/actionTypes'
  
export default function cardsReducer(state = [], action) {
    switch (action.type) {
      case GET_CARDS:
        return [...state, ...action.payload]
      case CREATE_CARD:
        return [...state, action.payload]
      case UPDATE_CARD:
        const indexOfUpdatedCard = state.findIndex(
          card => card.id === action.payload.id
        )
        state.splice(indexOfUpdatedCard, 1)
        state.splice(indexOfUpdatedCard, 0, action.payload)
        return [...state]
      case DELETE_CARD:
        const indexOfDeletedCard= state.findIndex(
          card => card.id === action.id
        )
        state.splice(indexOfDeletedCard, 1)
        return [...state]
      default:
        return state
    }
}