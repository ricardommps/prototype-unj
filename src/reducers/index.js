import { combineReducers } from "redux";
import tagsReducer from './tagsReducer'
import cardsReducer from './cardsReducer'

const rootReducer = combineReducers({
  tags: tagsReducer,
  cards: cardsReducer   
});
  
export default rootReducer;