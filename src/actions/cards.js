import { API_URL } from '../api'
import { GET_CARDS, CREATE_CARD, UPDATE_CARD, DELETE_CARD } from './actionTypes'
import axios from 'axios'

export function fetchCards() {
    return dispatch => {
        axios
            .get(`${API_URL}/cards`)
            .then(response => dispatch(fetchCardsSuccess(response.data)))
            .catch(error => console.error('getAPICards failed:', error))
    }
}

export function fetchCardsSuccess(payload) {
    return { type: GET_CARDS, payload }
}

export function createCard(payload) {
    return dispatch => {
      axios
        .post(`${API_URL}/cards`, payload)
        .then(() => dispatch(createCardSuccess(payload)))
        .catch(error => console.error('createCard failed:', error))
    }
}
  
export function createCardSuccess(payload) {
    return { type: CREATE_CARD, payload }
}

export function updateCard(card,id) {
    return dispatch => {
      axios
        .put(`${API_URL}/cards/${id}`, card)
        .then(() => dispatch(updateCardSuccess(card)))
        .catch(error => console.error('updateCardfailed:', error))
    }
  }
  
export function updateCardSuccess(payload) {
    return { type: UPDATE_CARD, payload }
}

export function deleteCard(id) {
    return dispatch => {
      axios
        .delete(`${API_URL}/cards/${id}`)
        .then(() => dispatch(deleteCardSuccess(id)))
        .catch(error => console.error('deleteCard failed:', error))
    }
  }
  
export function deleteCardSuccess(id) {
    return { type: DELETE_CARD, id }
}

