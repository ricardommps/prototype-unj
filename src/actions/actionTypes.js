export const GET_TAGS = 'GET_TAGS'
export const CREATE_TAG = 'CREATE_TAG'
export const UPDATE_TAG = 'UPDATE_TAG'
export const DELETE_TAG = 'DELETE_TAG'

export const GET_CARDS = 'GET_CARDS'
export const CREATE_CARD = 'CREATE_CARD'
export const UPDATE_CARD = 'UPDATE_CARD'
export const DELETE_CARD = 'DELETE_CARD'
