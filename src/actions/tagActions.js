import { API_URL } from '../api'
import { GET_TAGS, CREATE_TAG, UPDATE_TAG, DELETE_TAG } from './actionTypes'
import axios from 'axios'

export function fetchTags() {
    return dispatch => {
        axios
            .get(`${API_URL}/tags`)
            .then(response => dispatch(fetchTagsSuccess(response.data)))
            .catch(error => console.error('getAPITags failed:', error))
    }
}

export function fetchTagsSuccess(payload) {
    return { type: GET_TAGS, payload }
}

export function createTag(payload) {
    return dispatch => {
      axios
        .post(`${API_URL}/tags`, payload)
        .then(() => dispatch(createTagSuccess(payload)))
        .catch(error => console.error('createTag failed:', error))
    }
}
  
export function createTagSuccess(payload) {
    return { type: CREATE_TAG, payload }
}

export function updateTag(payload) {
    return dispatch => {
      axios
        .put(`${API_URL}/tags/${payload.id}`, payload)
        .then(() => dispatch(updateTagSuccess(payload)))
        .catch(error => console.error('updateTag failed:', error))
    }
  }
  
export function updateTagSuccess(payload) {
    return { type: UPDATE_TAG, payload }
}

export function deleteTag(id) {
    return dispatch => {
      axios
        .delete(`${API_URL}/tags/${id}`)
        .then(() => dispatch(deleteTagSuccess(id)))
        .catch(error => console.error('deleteTag failed:', error))
    }
  }
  
export function deleteTagSuccess(id) {
    return { type: DELETE_TAG, id }
}

