import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom'
import history from './history'

import AdminPage from './components/AdminPage'

const App = () => (
    <Router history={history}>
      <div>
        <Switch>
            <Route exact path="/" component={AdminPage} />
        </Switch>
      </div>
    </Router>
)

export default App;
