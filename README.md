## Clone project
```bash
git clone https://ricardommps@bitbucket.org/ricardommps/prototype-unj.git
```

##  How to install
```bash
cd prototype-unj
yarn
```

### Start the fake backend server
Start the backend server first:

```bash
yarn start:api
```
will start the `json-server` which will load the `./db.json`

### Start the client
Open a separate terminal to start the backend:

```bash
cd prototype-unj
yarn start:dev
```